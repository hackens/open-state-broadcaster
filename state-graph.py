"""
Interface d'un plugin :
    -> plug(stategraph)
    -> notify()
"""
class StateGraph:
    def __init__(self, states, events, graph, initialState):
        assert initialState in states
        self._states = states
        self._events = events
        self._graph = graph
        self._plugin = set()
        self._state = initialState

    def allowedTransition(self, event):
        return (self.state, event) in self._graph

    def transition(self, event):
        assert (self.state, event) in self._graph
        self.state = self._graph[(self.state, event)]
        self._notify()

    def _notify(self):
        for p in self._plugin:
            p.notify()

    def plug(self, plugin):
        self._plugin.add(plugin)
        plugin.plug(self)

    @property
    def state(self):
        return self._state

    @state.setter
    def set_state(self, state):
        self._state = state
        self._notify()

