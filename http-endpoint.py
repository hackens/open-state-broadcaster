from flask import Flask

def HttpHandler:
    def __init__(self, path = "/", publicStateModifier = None):
        """
        `path` must end with a `/`
        """
        self._path = path
        self._app = Flask(__name__)
        self._stateGraph = None
        self.publicStateModifier = publicStateModifier

    @property
    def path(self):
        return self._path

    @path.setter
    def set_path(self, path):
        raise AttributeError("Handler path can't be changed")

    def run(self, host=None, port=None, debug=None, load_dotenv=True, **options):
        self._app.run(host=None, port=None, debug=None, load_dotenv=True, **options)

    def getPublic(self):
        if self.publicStateModifier is not None:
            return self.publicStateModifier(self._stateGraph.state)
        else:
            return self._stateGraph.state

    def notify(self):
        pass

    def plug(self, stateGraph):
        self._stateGraph = stateGraph
        @self.app.route(f"{path}<string:event>", methods = ['POST'])
        def transition(event):
            if self._stateGraph.allowedTransition(event) and self._authorized(request.args.get("token", default=""), rights = 2):
                self._stateGraph.transition(event)
                return Response(status=200)
            else:
                return Response(status=403)
        @self.app.route(f"{path}public-state", methods = ['GET'])
        def getPublic():
            return { "public-state": self.getPublic() }
        @self.app.route(f"{path}private-state", methods = ['GET'])
        def getPrivate():
            if self._authorized(request.args.get("token", default=""), rights = 1):
                return { "private-state": self.getPublic() }
            else:
                return Response(status=403)

    def _authorized(self, token):
        return token == "debug"
